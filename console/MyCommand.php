<?php namespace Summer\AutobotSocial\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Summer\AutobotSocial\Datasources\Twitter;

class MyCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'autobotsocial:mycommand';

    /**
     * @var string The console command description.
     */
    protected $description = 'Does something cool.';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
      $twitter = new Twitter;
      echo $twitter->follow("ODEM_IO");



    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
