<?php namespace Summer\AutobotSocial\Api\V1;
use  Symfony\Component\HttpFoundation\Request;
use Summer\QueueManager\Classes\ApiBase;

use Summer\Autobotsocial\Models\CryptoPingSignal;
use Summer\Autobotsocial\Models\SignalStatistic;

class CryptoPingAPI extends ApiBase {
  public function __construct(){
    parent::__construct("path","GET");

  }

  public function handleRequest(Request $request){


    $ticker = $this->getInput("ticker");
    $exchange = strtoupper($this->getInput("exchange"));
    $type = strtolower($this->getInput("type"));
    $price_btc = $this->getInput("price_btc");
    $volume_btc_change = $this->getInput("volume_btc_change");
    $volume_pct_change = $this->getInput("volume_pct_change");
    $price_pct_change = $this->getInput("price_pct_change");
    $btc_usd = $this->getInput("btc_usd");
    $count = $this->getInput("count");
    $signal_at = $this->getInput("created_at");


    $signal = new CryptoPingSignal;

    $signal_statistic = new SignalStatistic;
    $signal_statistic->coin = $ticker;
    $signal_statistic->broker_code = $exchange;
    $signal_statistic->checkpoint_time = time();

    if($signal_statistic->save()){
      $signal->signal_statistic_link = $signal_statistic->id;
    }


    $signal->ticker = $ticker;
    $signal->exchange = $exchange;
    $signal->type = $type;
    $signal->price_btc = $price_btc;
    $signal->volume_btc_change = $volume_btc_change;
    $signal->volume_pct_change = $volume_pct_change;
    $signal->price_pct_change = $price_pct_change;
    $signal->btc_usd = $btc_usd;
    $signal->count = $count;
    $signal->signal_at =  date("Y-m-d H:i:s", strtotime($signal_at));
    $signal->save();

    echo "OK";

  }

}
