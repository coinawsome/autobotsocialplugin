<?php namespace Summer\AutobotSocial\Api\V1;
use  Symfony\Component\HttpFoundation\Request;
use Summer\QueueManager\Classes\ApiBase;

use Summer\Autobotsocial\Models\TelegramMessage;
use Summer\Autobotsocial\Models\SignalStatistic;

class TelegramAPI extends ApiBase {
  public function __construct(){
    parent::__construct("path","GET");

  }

  public function handleRequest(Request $request){

    $message = $this->getInput("message");
    $channel_id = $this->getInput("channel_id");
    $id = $this->getInput("id");

    $ignore_list = array('today','minutes','join','awesome','promo','guys');
    foreach ($ignore_list as $ignore_keyword) {
      if(stristr($message,$ignore_keyword)){
        echo "IGNORE";
        return ;
      }
    }



    $telegram = new TelegramMessage;
    $telegram->message = $message;
    $telegram->channel_id = $channel_id;
    $telegram->message_id = $id;

    $telegram->save();


    echo "OK";

  }
}
