<?php namespace Summer\AutobotSocial\Datasources;

use Sunra\PhpSimple\HtmlDomParser;
use Summer\AutobotSocial\Utils\CommonUtils;
class CoinMarketCap {


  public function __construct($coin_id){
    $this->coin_id = $coin_id;
    $this->getHtml();
  }

  //Return array
  public function getTwitters(){
    $list = array();
    if($this->dom){

      foreach($this->dom->find('.twitter-timeline') as $element){
        $list[] = $element->href;
      }
    }
    return $list;
  }

  private function getHtml(){
    $html = CommonUtils::curl("https://coinmarketcap.com/currencies/".$this->coin_id."/");
    $dom = HtmlDomParser::str_get_html( $html );
    $this->dom = $dom;

  }

}
