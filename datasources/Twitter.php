<?php namespace Summer\AutobotSocial\Datasources;
use Carbon\Carbon;
use Queue;
use DB;

use Summer\AutobotSocial\Libs\TweetPHP\TweetPHP;
use Summer\Autobotsocial\Models\TwitterSource;
use Summer\Autobotsocial\Models\TwitterFeed;
use Summer\Autobotsocial\Models\SignalStatistic;
class Twitter {

  public function __construct(){
    $this->config = array(
      'consumer_key'        => config('autobotsocial.twitter.consumer_key'),
      'consumer_secret'     => config('autobotsocial.twitter.consumer_secret'),
      'access_token'        => config('autobotsocial.twitter.access_token'),
      'access_token_secret' => config('autobotsocial.twitter.access_token_secret'),
      'enable_cache'        =>  false
    );

  }

  public function follow($screen_name){

    $config = array_merge(array(
      'api_endpoint'          => 'friendships/create',
      'api_params'          =>  array('screen_name'=>$screen_name,'follow'=>true),
    ),$this->config);


    $TweetPHP = new TweetPHP($config);
    $status = $TweetPHP->follow();
    if($status == 200){
      return true;
    }
    return false;

  }

  public function getFeed(){

    $latest_feed = TwitterFeed::max('id');
    if($latest_feed ==0){
      $latest_feed = 1;
    }
    $config = array_merge(array(
      'api_endpoint'          => 'statuses/home_timeline',
      'api_params'          =>  array('since_id'=>$latest_feed),
      'tweets_to_retrieve'  =>  200
    ),$this->config);


    $TweetPHP = new TweetPHP($config);

    $tweet_array = $TweetPHP->get_tweet_array();

    $now = time();
    foreach ($tweet_array as $tweet) {

      $feed  = TwitterFeed::where('feed_id',$tweet['id'])->first();
      if(!$feed){
        $screen_name = $tweet['user']['screen_name'];
        $source = TwitterSource::where('twitter_id',$screen_name)->first();
        if(!$source){
          echo "Can not find Screen name : $screen_name\n";
          continue;
        }


        $feed = new TwitterFeed;
        $feed->feed_id = $tweet['id'];
        $feed->twitter_source = $source->id;
        $feed->feed_text = $tweet['text'];
        $feed->coin_id = $source->coin_id;

        $tweet_time = strtotime($tweet['created_at']);

        if ($now - 7*24*60*60 > $tweet_time){
          $feed->isread = 1;

        }


        $feed->tweet_at = date("Y-m-d H:i:s", strtotime($tweet['created_at']));

        if($feed->save()){

          if ($now - 7*24*60*60 < $tweet_time){
            $signal_statistic = new SignalStatistic;
            $signal_statistic->coin_id = $source->coin_id;
            $signal_statistic->checkpoint_time = time();

            if($signal_statistic->save()){
              $feed->signal_statistic_link = $signal_statistic->id;
              $feed->save();
            }

          }

        }

      }

    }

  }

}
