<?php namespace Summer\AutobotSocial\Utils;

class CommonUtils {
  public static function curl($url){

      // create curl resource
       $ch = curl_init();

       // set url
       curl_setopt($ch, CURLOPT_URL, $url);

       //return the transfer as a string
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
       curl_setopt($ch, CURLOPT_TIMEOUT, 10);

       // $output contains the output string
       $output = curl_exec($ch);

       // close curl resource to free up system resources
       curl_close($ch);

       return $output;

  }



}
