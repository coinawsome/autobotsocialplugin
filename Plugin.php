<?php namespace Summer\Autobotsocial;

use System\Classes\PluginBase;
use Event;

use Summer\AutobotSocial\Events\GetCoinMarketCapEvent;
use Summer\AutobotSocial\Events\GetPriceEvent;
use Summer\AutobotSocial\Schedules\ScheduleManager;
class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function registerSchedule($schedule)
    {
        ScheduleManager::schedule($schedule);
    }

    public function register()
    {
        $this->registerConsoleCommand('autobotsocial.mycommand', 'Summer\AutobotSocial\Console\MyCommand');
    }

    public function boot()
    {
      Event::listen('autobroker.getcoinmarketcap', function() {
        GetCoinMarketCapEvent::onGetMarketCapDone();

      });
      Event::listen('autobroker.getprice', function($broker_code) {
        GetPriceEvent::onGetPriceDone($broker_code);

      });

    }
}
