<?php namespace Summer\Autobotsocial\Models;

use Model;

/**
 * Model
 */
class TwitterFeed extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'summer_autobotsocial_twitter_feed';

    public $belongsTo = [
        'statistic' => ['Summer\Autobotsocial\Models\SignalStatistic','key'=>'signal_statistic_link']
    ];
    public function getPercentChangeAttribute(){
      if($this->signal_statistic_link ==0) {
        return 0;
      }

      $statistic = $this->statistic;
      if($statistic->begin_price ==0){
        return 0;
      }

      return (($statistic->current_price - $statistic->begin_price ) / $statistic->begin_price )*100 . " %";



    }
}
