<?php namespace Summer\Autobotsocial\Models;

use Model;

/**
 * Model
 */
class TelegramMessage extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'summer_autobotsocial_telegram';

    public function afterFetch(){
      $this->message = base64_decode($this->message);
    }
    public function beforeSave(){
      $this->message=base64_encode($this->message);
    }
}
