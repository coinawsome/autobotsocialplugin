<?php namespace Summer\Autobotsocial\Models;

use Model;

/**
 * Model
 */
class TwitterSource extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'summer_autobotsocial_twitter_source';

    public function getCoinIdOptions(){
      return array();
    }
}
