<?php
namespace Summer\AutobotSocial\Schedules;

use Event;
use Summer\AutobotSocial\Queues\QueueManager;

class ScheduleManager {


  public static function schedule($schedule){
    $schedule->call(function () {
      QueueManager::startQueues();
    })->everyMinute();

  }

}

?>
