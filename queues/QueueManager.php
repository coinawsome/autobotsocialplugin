<?php
namespace Summer\AutobotSocial\Queues;

use Event;
use Queue;
use Carbon\Carbon;
use Cache;

use Summer\AutobotSocial\Models\TwitterSource;
use Summer\Autobroker\Models\MarketCapCoin;
class QueueManager {

  public static function startQueues($queue_name=false){


    sleep(rand(0,5));
    if(!QueueManager::acquire_lock_by_cache("autobotsocial.queue_manager",30)){

      return false;
    }


    QueueManager::queueTwitterFeeds();
    QueueManager::queueGetTwitterChannels();
    QueueManager::followOnTwitters();



  }

  private static function acquire_lock_by_cache($key,$seconds_to_lock){

    if (Cache::has($key)) {

      return false;
    }

    $expiresAt = Carbon::now()->addSeconds($seconds_to_lock);

    Cache::put($key, 1 , $expiresAt);
    return true;

  }

  private static function queueGetTwitterChannels(){

    $current_minute = date("i");
    $current_hour = date("H");
    if ($current_minute != 0 &&  $current_hour!= 0 ){
      return ;
    }
    $coins = MarketCapCoin::get();
    $index = 0;
    foreach ($coins as $coin) {
      $index++;

      $parameters = array();
      $parameters['coin_name'] = $coin->name;
      $parameters['coin_id'] = $coin->coin_id;

      $date = Carbon::now()->addSeconds($index);
      Queue::later($date, "Summer\AutobotSocial\Queues\CoinMarketCapGetSocialQueue", $parameters );

    }

  }

  private static function queueTwitterFeeds(){

    $current_minute = date("i");
    if ($current_minute % 10 != 0 ){
      return ;
    }

    $parameters = array();
    Queue::push("Summer\AutobotSocial\Queues\TwitterFeedQueue", $parameters);


  }

  private static function followOnTwitters(){
    $current_minute = date("i");
    if ($current_minute % 10 != 1 ){
      return ;
    }

    $twitter_sources = TwitterSource::where('followed',0)->take(10)->get();
    $index = 0;
    foreach ($twitter_sources as $twitter_source) {
      $index++;

      $parameters = array();
      $parameters['id'] = $twitter_source->id;

      $date = Carbon::now()->addSeconds($index*60);
      Queue::later($date, "Summer\AutobotSocial\Queues\TwitterFollowQueue", $parameters );

    }
  }


}

?>
