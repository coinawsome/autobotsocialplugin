<?php namespace Summer\AutobotSocial\Queues;
use Event;
use Summer\QueueManager\Classes\QueueBase;
use  Summer\AutobotSocial\Models\TwitterSource;
use Summer\AutobotSocial\Datasources\Twitter;

class TwitterFollowQueue extends QueueBase{
  public function __construct(){
    parent::__construct("AutobotSocial.TwitterFollow",0);

  }


  public function acquire_lock ($data){
    $id = $data['id'];
    $key = "TwitterFollow_".$id;
    return $this->acquire_lock_by_cache($key,60);
  }



  public function queueHandler($job, $data)
  {
    $id = $data['id'];

    $twitter_source = TwitterSource::where('id',$id)->first();
    if($twitter_source){
      $twitter = new Twitter;
      $status = $twitter->follow($twitter_source->twitter_id);
      if($status){
        $twitter_source->followed = 1;
        $twitter_source->save();
      }
    }


  }




}
