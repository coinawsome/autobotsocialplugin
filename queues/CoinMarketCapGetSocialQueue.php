<?php namespace Summer\AutobotSocial\Queues;
use Event;
use Summer\QueueManager\Classes\QueueBase;
use Summer\AutobotSocial\Datasources\CoinMarketCap;
use Summer\Autobotsocial\Models\TwitterSource;

class CoinMarketCapGetSocialQueue extends QueueBase{
  public function __construct(){
    parent::__construct("AutobotSocial.CoinMarketCapGetSocial",0);

  }


  public function acquire_lock ($data){
    $coin_id = $data['coin_id'];
    $key = "CoinMarketCapGetSocialQueue_".$coin_id;
    return $this->acquire_lock_by_cache($key,24*60*60);
  }



  public function queueHandler($job, $data)
  {

    $coin_name = $data['coin_name'];
    $coin_id = $data['coin_id'];
    $mc = new CoinMarketCap($coin_id);

    $twitters = $mc->getTwitters();
    $name = "Twitter MC : $coin_name";
    foreach ($twitters as $twitter) {
      try{
        $twitter_id = str_replace("https://twitter.com/","",$twitter);

        $twitter_source = TwitterSource::where('twitter_id',$twitter_id)->first();
        if(!$twitter_source){
          $twitter_source = new TwitterSource;
          $twitter_source->name = $name;
          $twitter_source->coin_id = $coin_id;
          $twitter_source->type = "USER";
          $twitter_source->twitter_id =$twitter_id;
          $twitter_source->save();

        }




      }catch (Exception $e) {
        echo $e->getMessage();
      }
    }


  }
}
