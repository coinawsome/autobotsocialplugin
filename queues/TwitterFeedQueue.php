<?php namespace Summer\AutobotSocial\Queues;
use Event;
use Summer\QueueManager\Classes\QueueBase;
use  Summer\AutobotSocial\Models\TwitterSource;
use Summer\AutobotSocial\Datasources\Twitter;

class TwitterFeedQueue extends QueueBase{
  public function __construct(){
    parent::__construct("AutobotSocial.TwitterCrawler",0);

  }


  public function acquire_lock ($data){
    $key = "TwitterFeedQueue";
    return $this->acquire_lock_by_cache($key,60);
  }



  public function queueHandler($job, $data)
  {

    $twitter = new Twitter;
    $twitter->getFeed();

  }




}
