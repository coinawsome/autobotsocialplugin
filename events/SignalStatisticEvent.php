<?php namespace Summer\AutobotSocial\Events;
use Event;
use Carbon\Carbon;
use DB;
use Queue;
use Summer\Autobotsocial\Models\CryptoPingSignal;
use Summer\Autobotsocial\Models\SignalStatistic;

use Summer\Autobotsocial\Models\TwitterFeed;

class SignalStatisticEvent {

  public static function onSignalStatisticUpdate($broker_code){

    if($broker_code == 'COINMARKETCAP'){
      SignalStatisticEvent::onSignalStatisticUpdateMarketCap();
    }else{
      SignalStatisticEvent::onSignalStatisticUpdateBroker($broker_code);

    }

  }

  private static function onSignalStatisticUpdateMarketCap(){
    $signalStatistic_table = (new SignalStatistic)->getTable();
    $twitterFeed_table = (new TwitterFeed)->getTable();

    $now = Carbon::now();

    $query = "UPDATE $twitterFeed_table twitterFeed
              JOIN $signalStatistic_table signalstatistic
              ON twitterFeed.signal_statistic_link = signalstatistic.id

              SET
              twitterFeed.current_percent = 100*(signalstatistic.current_price - signalstatistic.begin_price ) / signalstatistic.begin_price,
              twitterFeed.max_percent = 100*(signalstatistic.max_price - signalstatistic.begin_price ) / signalstatistic.begin_price,
              twitterFeed.min_percent = 100*(signalstatistic.min_price - signalstatistic.begin_price ) / signalstatistic.begin_price,
              twitterFeed.status = signalstatistic.status,
              twitterFeed.updated_at = '$now'
              WHERE twitterFeed.status =1 AND twitterFeed.signal_statistic_link >0
              ";

    DB::update($query);

  }

  private static function onSignalStatisticUpdateBroker($broker_code){
    $signalStatistic_table = (new SignalStatistic)->getTable();
    $cryptoping_table = (new CryptoPingSignal)->getTable();

    $now = Carbon::now();

    $query = "UPDATE $cryptoping_table cryptoping
              JOIN $signalStatistic_table signalstatistic
              ON cryptoping.signal_statistic_link = signalstatistic.id

              SET
              cryptoping.current_percent = 100*(signalstatistic.current_price - signalstatistic.begin_price ) / signalstatistic.begin_price,
              cryptoping.max_percent = 100*(signalstatistic.max_price - signalstatistic.begin_price ) / signalstatistic.begin_price,
              cryptoping.min_percent = 100*(signalstatistic.min_price - signalstatistic.begin_price ) / signalstatistic.begin_price,
              cryptoping.status = signalstatistic.status,
              cryptoping.updated_at = '$now'
              WHERE cryptoping.status =1 AND cryptoping.signal_statistic_link >0 AND exchange='$broker_code'
              ";

    DB::update($query);

  }


}
