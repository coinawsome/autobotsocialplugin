<?php namespace Summer\AutobotSocial\Events;
use Event;
use Carbon\Carbon;
use DB;
use Queue;

use Summer\Autobotsocial\Models\SignalStatistic;
use Summer\Autobroker\Models\CoinPrice;

use Summer\AutobotSocial\Events\SignalStatisticEvent;
class GetPriceEvent {
  public static function onGetPriceDone($broker_code){

    $signalStatistic_table = (new SignalStatistic)->getTable();
    $coinprice_table = (new CoinPrice)->getTable();

    $now=time();
    $updated_at = Carbon::now();
    $query = "
      UPDATE $signalStatistic_table statistic
      JOIN $coinprice_table coinprice
      ON statistic.coin = coinprice.coin AND statistic.broker_code = coinprice.broker_code

      SET
        statistic.updated_at = '$updated_at',
        begin_price = IF(begin_price=0,usd_last_price,begin_price),
        current_price = usd_last_price,
        usd_min_1h = IF((checkpoint_time+60*60>$now AND usd_last_price < usd_min_1h) OR usd_min_1h=0  ,usd_last_price,usd_min_1h),
        usd_min_6h = IF((checkpoint_time+6*60*60>$now AND usd_last_price< usd_min_6h) OR usd_min_6h=0  ,usd_last_price,usd_min_6h),
        usd_min_12h = IF((checkpoint_time+12*60*60>$now AND usd_last_price< usd_min_12h) OR usd_min_12h=0,usd_last_price,usd_min_12h),
        usd_min_1d = IF((checkpoint_time+24*60*60>$now AND usd_last_price< usd_min_1d) OR usd_min_1d=0,usd_last_price,usd_min_1d),
        usd_min_3d = IF((checkpoint_time+3*24*60*60>$now AND usd_last_price< usd_min_3d) OR usd_min_3d=0,usd_last_price,usd_min_3d),
        usd_min_7d = IF((checkpoint_time+7*24*60*60>$now AND usd_last_price< usd_min_7d) OR usd_min_7d=0 ,usd_last_price,usd_min_7d),

        usd_max_1h = IF(checkpoint_time+60*60>$now AND usd_last_price > usd_max_1h ,usd_last_price,usd_max_1h),
        usd_max_6h = IF(checkpoint_time+6*60*60>$now AND usd_last_price > usd_max_6h ,usd_last_price,usd_max_6h),
        usd_max_12h = IF(checkpoint_time+12*60*60>$now AND usd_last_price> usd_max_12h,usd_last_price,usd_max_12h),
        usd_max_1d = IF(checkpoint_time+24*60*60>$now AND usd_last_price> usd_max_1d,usd_last_price,usd_max_1d),
        usd_max_3d = IF(checkpoint_time+3*24*60*60>$now AND usd_last_price> usd_max_3d,usd_last_price,usd_max_3d),
        usd_max_7d = IF(checkpoint_time+7*24*60*60>$now AND usd_last_price> usd_max_7d,usd_last_price,usd_max_7d),
        max_price  = IF(usd_last_price > max_price , usd_last_price,max_price),
        min_price = IF(min_price=0 OR usd_last_price < min_price , usd_last_price,min_price)

      WHERE statistic.status=1 AND statistic.broker_code='$broker_code';

    ";

    DB::statement($query);


    $sevenDaysAgo = $updated_at->subDays(7);
    $query = "UPDATE $signalStatistic_table SET status = 0 WHERE broker_code='$broker_code' AND created_at < '$sevenDaysAgo'";
    DB::update($query);

    SignalStatisticEvent::onSignalStatisticUpdate($broker_code);




  }




}
