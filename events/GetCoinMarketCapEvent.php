<?php namespace Summer\AutobotSocial\Events;
use Event;
use DB;
use Carbon\Carbon;
use Summer\Autobotsocial\Models\SignalStatistic;
use Summer\Autobroker\Models\MarketCapCoin;
use Summer\AutobotSocial\Events\SignalStatisticEvent;
class GetCoinMarketCapEvent {

  public static function onGetMarketCapDone(){
    /*
    Fire Event when get Market Cap Done
    */


    $signalStatistic_table = (new SignalStatistic)->getTable();
    $coinMarketcap_table = (new MarketCapCoin)->getTable();
    $now=time();
    $updated_at = Carbon::now();
    $query = "
      UPDATE $signalStatistic_table statistic
      JOIN $coinMarketcap_table market
      ON statistic.coin_id = market.coin_id

      SET
        statistic.updated_at = '$updated_at',
        begin_price = IF(begin_price=0,price_usd,begin_price),
        current_price = price_usd,
        usd_min_1h = IF((checkpoint_time+60*60>$now AND price_usd < usd_min_1h) OR usd_min_1h=0  ,price_usd,usd_min_1h),
        usd_min_6h = IF((checkpoint_time+6*60*60>$now AND price_usd< usd_min_6h) OR usd_min_6h=0  ,price_usd,usd_min_6h),
        usd_min_12h = IF((checkpoint_time+12*60*60>$now AND price_usd< usd_min_12h) OR usd_min_12h=0,price_usd,usd_min_12h),
        usd_min_1d = IF((checkpoint_time+24*60*60>$now AND price_usd< usd_min_1d) OR usd_min_1d=0,price_usd,usd_min_1d),
        usd_min_3d = IF((checkpoint_time+3*24*60*60>$now AND price_usd< usd_min_3d) OR usd_min_3d=0,price_usd,usd_min_3d),
        usd_min_7d = IF((checkpoint_time+7*24*60*60>$now AND price_usd< usd_min_7d) OR usd_min_7d=0 ,price_usd,usd_min_7d),

        usd_max_1h = IF(checkpoint_time+60*60>$now AND price_usd > usd_max_1h ,price_usd,usd_max_1h),
        usd_max_6h = IF(checkpoint_time+6*60*60>$now AND price_usd > usd_max_6h ,price_usd,usd_max_6h),
        usd_max_12h = IF(checkpoint_time+12*60*60>$now AND price_usd> usd_max_12h,price_usd,usd_max_12h),
        usd_max_1d = IF(checkpoint_time+24*60*60>$now AND price_usd> usd_max_1d,price_usd,usd_max_1d),
        usd_max_3d = IF(checkpoint_time+3*24*60*60>$now AND price_usd> usd_max_3d,price_usd,usd_max_3d),
        usd_max_7d = IF(checkpoint_time+7*24*60*60>$now AND price_usd> usd_max_7d,price_usd,usd_max_7d),
        max_price  = IF(price_usd > max_price , price_usd,max_price),
        min_price = IF(min_price=0 OR price_usd < min_price , price_usd,min_price)

      WHERE statistic.status=1 AND broker_code='COINMARKETCAP';

    ";

    DB::statement($query);

    $sevenDaysAgo = $updated_at->subDays(7);

    $query = "UPDATE $signalStatistic_table SET status = 0 WHERE broker_code='COINMARKETCAP' AND created_at < '$sevenDaysAgo'";
    DB::update($query);

    SignalStatisticEvent::onSignalStatisticUpdate('COINMARKETCAP');






  }


}
