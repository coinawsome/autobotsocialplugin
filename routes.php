<?php
use Summer\AutobotSocial\Api\V1\CryptoPingAPI;
use Summer\AutobotSocial\Api\V1\TelegramAPI;

Route::group(['prefix' => 'api/autobotsocial'], function () {
     Route::group(['prefix' => 'v1'], function () {
       Route::post('cryptoping', 'Summer\AutobotSocial\Api\V1\CryptoPingAPI@handle' );
       Route::post('telegram', 'Summer\AutobotSocial\Api\V1\TelegramAPI@handle' );
     });
 });

 ?>
