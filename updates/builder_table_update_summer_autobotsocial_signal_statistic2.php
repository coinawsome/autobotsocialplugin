<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSummerAutobotsocialSignalStatistic2 extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_signal_statistic', function($table)
        {
            $table->decimal('max_price', 19, 9)->default(0);
            $table->decimal('min_price', 19, 9)->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_signal_statistic', function($table)
        {
            $table->dropColumn('max_price');
            $table->dropColumn('min_price');
        });
    }
}