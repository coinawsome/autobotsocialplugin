<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSummerAutobotsocialTwitterSource extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_twitter_source', function($table)
        {
            $table->string('name', 128)->nullable();
            $table->string('coin_id', 64)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_twitter_source', function($table)
        {
            $table->dropColumn('name');
            $table->dropColumn('coin_id');
        });
    }
}
