<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSummerAutobotsocialTwitterFeed2 extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_twitter_feed', function($table)
        {
            $table->renameColumn('twitter_user', 'twitter_source');
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_twitter_feed', function($table)
        {
            $table->renameColumn('twitter_source', 'twitter_user');
        });
    }
}
