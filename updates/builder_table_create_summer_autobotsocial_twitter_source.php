<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSummerAutobotsocialTwitterSource extends Migration
{
    public function up()
    {
        Schema::create('summer_autobotsocial_twitter_source', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('type', 32);
            $table->string('twitter_id', 64);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->boolean('status')->default(1);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('summer_autobotsocial_twitter_source');
    }
}
