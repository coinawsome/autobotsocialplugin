<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSummerAutobotsocialCryptoping3 extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_cryptoping', function($table)
        {
            $table->boolean('status')->default(1);
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_cryptoping', function($table)
        {
            $table->dropColumn('status');
        });
    }
}