<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSummerAutobotsocialTelegram extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_telegram', function($table)
        {
            $table->integer('message_id');
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_telegram', function($table)
        {
            $table->dropColumn('message_id');
        });
    }
}