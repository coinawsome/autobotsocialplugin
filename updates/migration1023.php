<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1023 extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_signal_statistic', function($table)
        {
            $table->index("coin");
            $table->index("broker_code");

        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_signal_statistic', function($table)
        {
            $table->dropIndex("summer_autobotsocial_signal_statistic_coin_index");
            $table->dropIndex("summer_autobotsocial_signal_statistic_broker_code_index");
        });
    }
}