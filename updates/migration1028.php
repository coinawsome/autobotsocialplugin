<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1028 extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_cryptoping', function($table)
        {
            $table->index("status");
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_cryptoping', function($table)
        {
            $table->dropIndex("summer_autobotsocial_cryptoping_status_index");
        });
    }
}