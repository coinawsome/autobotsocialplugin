<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSummerAutobotsocialTwitterFeed_6 extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_twitter_feed', function($table)
        {
            $table->decimal('max_percent', 10, 4)->default(0);
            $table->decimal('min_percent', 10, 4)->default(0);
            $table->decimal('current_percent', 10, 4)->default(0);
        });
    }

    public function down()
    {
        Schema::table('summer_autobotsocial_twitter_feed', function($table)
        {
            $table->dropColumn('max_percent');
            $table->dropColumn('min_percent');
            $table->dropColumn('current_percent');
        });
    }
}
