<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration109 extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_twitter_feed', function($table)
        {
            $table->unique("feed_id");
            $table->index("isread");
            $table->index("twitter_source");
            $table->index("tweet_at");
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_twitter_feed', function($table)
        {
          $table->dropUnique("summer_autobotsocial_twitter_feed_feed_id_unique");
          $table->dropIndex("summer_autobotsocial_twitter_feed_isread_index");
          $table->dropIndex("summer_autobotsocial_twitter_feed_twitter_source_index");
          $table->dropIndex("summer_autobotsocial_twitter_feed_tweet_at_index");
        });
    }
}