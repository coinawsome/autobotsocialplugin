<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1022 extends Migration
{
   public function up()
    {
        Schema::table('summer_autobotsocial_cryptoping', function($table)
        {
            $table->index("ticker");
            $table->index("exchange");
            $table->index("type");
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_cryptoping', function($table)
        {
            $table->dropIndex("summer_autobotsocial_cryptoping_ticker_index");
            $table->dropIndex("summer_autobotsocial_cryptoping_exchange_index");
            $table->dropIndex("summer_autobotsocial_cryptoping_type_index");
        });
    }
}