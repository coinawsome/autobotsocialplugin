<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSummerAutobotsocialSignalStatistic_4 extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_signal_statistic', function($table)
        {
            $table->integer('checkpoint_time')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('summer_autobotsocial_signal_statistic', function($table)
        {
            $table->integer('checkpoint_time')->nullable(false)->change();
        });
    }
}
