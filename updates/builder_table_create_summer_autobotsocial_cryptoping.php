<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSummerAutobotsocialCryptoping extends Migration
{
    public function up()
    {
        Schema::create('summer_autobotsocial_cryptoping', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('ticker', 64);
            $table->string('exchange', 16);
            $table->string('type', 8);
            $table->decimal('price_btc', 19, 9)->default(0);
            $table->decimal('volume_btc_change', 19, 9)->default(0);
            $table->decimal('volume_pct_change', 19, 9)->default(0);
            $table->decimal('price_pct_change', 19, 9)->default(0);
            $table->decimal('btc_usd', 19, 9);
            $table->integer('count');
            $table->dateTime('signal_at');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('summer_autobotsocial_cryptoping');
    }
}