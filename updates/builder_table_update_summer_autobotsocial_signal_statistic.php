<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSummerAutobotsocialSignalStatistic extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_signal_statistic', function($table)
        {
            $table->decimal('begin_price', 19, 9)->default(0);
            $table->decimal('current_price', 19, 9)->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_signal_statistic', function($table)
        {
            $table->dropColumn('begin_price');
            $table->dropColumn('current_price');
        });
    }
}
