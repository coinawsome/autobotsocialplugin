<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSummerAutobotsocialSignalStatistic4 extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_signal_statistic', function($table)
        {
            $table->string('broker_code', 16)->default('COINMARKETCAP')->change();
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_signal_statistic', function($table)
        {
            $table->string('broker_code', 16)->default(null)->change();
        });
    }
}