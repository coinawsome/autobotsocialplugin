<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSummerAutobotsocialTwitterFeed extends Migration
{
    public function up()
    {
        Schema::create('summer_autobotsocial_twitter_feed', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('feed_id', 32);
            $table->text('feed_text');
            $table->string('tweet_at', 64);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('summer_autobotsocial_twitter_feed');
    }
}
