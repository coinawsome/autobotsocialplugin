<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSummerAutobotsocialTwitterFeed6 extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_twitter_feed', function($table)
        {
            $table->string('coin_id', 64);
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_twitter_feed', function($table)
        {
            $table->dropColumn('coin_id');
        });
    }
}