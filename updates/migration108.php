<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration108 extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_twitter_source', function($table)
        {
            $table->unique("twitter_id");
            $table->index("coin_id");
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_twitter_source', function($table)
        {
          $table->dropUnique("summer_autobotsocial_twitter_source_twitter_id_unique");
          $table->dropIndex("summer_autobotsocial_twitter_source_coin_id_index");
        });
    }
}