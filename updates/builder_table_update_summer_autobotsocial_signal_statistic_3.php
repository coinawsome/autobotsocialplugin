<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSummerAutobotsocialSignalStatistic3 extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_signal_statistic', function($table)
        {
            $table->string('coin', 8)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_signal_statistic', function($table)
        {
            $table->dropColumn('coin');
        });
    }
}
