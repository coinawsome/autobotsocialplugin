<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSummerAutobotsocialSignalStatistic extends Migration
{
    public function up()
    {
        Schema::create('summer_autobotsocial_signal_statistic', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('coin_id', 64)->nullable();
            $table->integer('checkpoint_time');
            $table->decimal('usd_min_1h', 19, 9)->default(0);
            $table->decimal('usd_min_6h', 19, 9)->default(0);
            $table->decimal('usd_min_12h', 19, 9)->default(0);
            $table->decimal('usd_min_1d', 19, 9)->default(0);
            $table->decimal('usd_min_3d', 19, 9)->default(0);
            $table->decimal('usd_min_7d', 19, 9)->default(0);
            $table->decimal('usd_max_1h', 19, 9)->default(0);
            $table->decimal('usd_max_6h', 19, 9)->default(0);
            $table->decimal('usd_max_12h', 19, 9)->default(0);
            $table->decimal('usd_max_1d', 19, 9)->default(0);
            $table->decimal('usd_max_3d', 19, 9)->default(0);
            $table->decimal('usd_max_7d', 19, 9)->default(0);


            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->boolean('status')->default(1);
        });
    }

    public function down()
    {
        Schema::dropIfExists('summer_autobotsocial_signal_statistic');
    }
}
