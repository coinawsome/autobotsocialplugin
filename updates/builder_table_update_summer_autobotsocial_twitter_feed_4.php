<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSummerAutobotsocialTwitterFeed4 extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_twitter_feed', function($table)
        {
            $table->integer('signal_statistic_link')->default(0);
        });
    }

    public function down()
    {
        Schema::table('summer_autobotsocial_twitter_feed', function($table)
        {
            $table->dropColumn('signal_statistic_link');
        });
    }
}
