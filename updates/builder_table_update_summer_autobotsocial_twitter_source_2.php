<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSummerAutobotsocialTwitterSource2 extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_twitter_source', function($table)
        {
            $table->boolean('followed')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_twitter_source', function($table)
        {
            $table->dropColumn('followed');
        });
    }
}
