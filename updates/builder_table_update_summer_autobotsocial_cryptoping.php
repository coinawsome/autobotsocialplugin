<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSummerAutobotsocialCryptoping extends Migration
{
    public function up()
    {
        Schema::table('summer_autobotsocial_cryptoping', function($table)
        {
            $table->integer('signal_statistic_link')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_cryptoping', function($table)
        {
            $table->dropColumn('signal_statistic_link');
        });
    }
}
