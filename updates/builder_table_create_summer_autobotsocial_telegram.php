<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSummerAutobotsocialTelegram extends Migration
{
    public function up()
    {
        Schema::create('summer_autobotsocial_telegram', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('channel_id', 42);
            $table->text('message');
            $table->boolean('status')->default(1);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('signal_statistic_link')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('summer_autobotsocial_telegram');
    }
}