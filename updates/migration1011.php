<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1011 extends Migration
{
   public function up()
    {
        Schema::table('summer_autobotsocial_signal_statistic', function($table)
        {
            $table->index("coin_id");
            $table->index("status");
            $table->index("checkpoint_time");
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_signal_statistic', function($table)
        {
            $table->dropIndex("summer_autobotsocial_signal_statistic_coin_id_index");
            $table->dropIndex("summer_autobotsocial_signal_statistic_status_index");
            $table->dropIndex("summer_autobotsocial_signal_statistic_checkpoint_time_index");
            
        });
    }
}