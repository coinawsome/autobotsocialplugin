<?php namespace Summer\Autobotsocial\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1032 extends Migration
{
        public function up()
    {
        Schema::table('summer_autobotsocial_twitter_source', function($table)
        {
            $table->index("followed");
        });
    }
    
    public function down()
    {
        Schema::table('summer_autobotsocial_twitter_source', function($table)
        {
            $table->dropIndex("summer_autobotsocial_twitter_source_followed_index");
        });
    }
}